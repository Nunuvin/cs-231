'''
 cs-231
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import turtle
import math
import random


def setUpCanvas(x,y,name):
    '''
        creates the turtle canvas and names the window
    '''
    turtle.setup(x,y)
    turtle.title(name)

def congratulate():
    '''
        congratulates the player with victory
    '''
    print('You won!')

def gameLoop(screenWidth,screenHeight):
    '''
        contains game related stuff
    '''
    def createData(turnrate,speed,x,y,facingAngle=0):
        '''
            creates default dictionary with information about obj
        '''
        data={
        'TURNANGLE':turnrate,
        'MOVESPEED':speed,
        'x':x,
        'y':y,
        'currentAngle':facingAngle
        }

        return data

    def updateCoord(instance):
        '''
            updates data based on new coord of turtle
        '''
        x=instance.xcor()
        y=instance.ycor()
        angle=instance.heading()

        return x,y,angle

    def moveInvisibly(instance,x,y,show=True):
        instance.hideturtle()
        instance.penup()
        instance.goto(x,y)
        instance.pendown()
        #for play turtles have to appear while ui does not
        if show==True:
            instance.showturtle()

    def randomPos(instance,screenWidth,screenHeight):
        #moves turtle to random pos and generates coords
        x=random.randint(0,screenWidth)
        y=random.randint(0,screenHeight)
        def convertToTurtle(value,dimension):
            '''
                according to turtle doc setworlddimension can
                distort angles therefore workaround is wiser

                converts to turtle coord system
                relation to middle
                greater=pos number (greater-halfDim)
                less =neg number (-self)
                same=0
            '''
            if value>dimension//2:
                value-=dimension//2
            elif value<dimension//2:
                value=value*(-1)
            else:
                value=0
            return value

        x=convertToTurtle(x,screenWidth)
        y=convertToTurtle(y,screenHeight)

        moveInvisibly(instance,x,y)
        return x,y


    def wallCollision(instance,data,screenWidth,screenHeight):
        '''
            if hit wall set random position
            whenever x,y are 0 or 499 they are
            on edges
        '''
        if (data['x']<=-249 or data['x']>=249 or
            data['y']<=-249 or data['y']>=249):
            x,y=randomPos(instance,screenWidth,screenHeight)

        else:
            x,y=data['x'],data['y']

        return x,y

    def calcDistance(dataA,dataB):
        '''
            calculates how far a is from b
            c**2=a**2+b**2
        '''
        length=math.sqrt((dataA['x']-dataB['x'])**2
                        +(dataA['y']-dataB['y'])**2)
        return length

    def catchedUp(length):
        '''
            checks to see if A is 30 px away from B
        '''
        conditionMet=False

        if length<=30:
            conditionMet=True

        return conditionMet

    def ui(uiTurtle,distance,gameData):
        '''
            displays turn # and distance
        '''
        moveInvisibly(uiTurtle,-239,225,False)
        uiTurtle.hideturtle()
        uiTurtle.clear()
        uiTurtle.write('Step#:%d. Distance between Alex and Alice: %f'
                    %(gameData['step'],distance))
        gameData['step']=gameData['step']+1

    def move(instance,data,decision):
        '''
            moves the instance of turtle based on decision
        '''
        if decision=='forward':
            instance.forward(data['MOVESPEED'])

        elif decision=='back':
            instance.back(data['MOVESPEED'])

        elif decision=='turnLeft':
            instance.left(data['TURNANGLE'])

        elif decision=='turnRight':
            instance.right(data['TURNANGLE'])

    def AliceAI(data):
        '''
            decides what next action Alice will do based on bias
        '''
        decision=random.randint(1,data['MOVECHANCE']+data['TURNCHANCE'])

        #processes the decision with intervals [,) 
        #inclusive as start @0
        #applying bias
        if decision<=data['TURNCHANCE']:
            decision=random.randint(0,1) #left or right
            if decision==0:
                nextMove='turnLeft'
            elif decision==1:
                nextMove='turnRight'
        else:
            nextMove='forward'
        return nextMove

    def userMove(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData):
        '''
            reads keypress and returns next action
        '''
        #call move with particular settings based on keyboard input
        #onkey takes FUNCTIONS WITH NO ARGUMENTS


        def AInCheck(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData):

            alexData['x'],alexData['y'],alexData['currentAngle']=updateCoord(Alex)
            wallCollision(Alex,alexData,screenWidth,screenHeight)

            AIMove=AliceAI(aliceData)
            move(Alice,aliceData,AIMove)
            aliceData['x'],aliceData['y'],aliceData['currentAngle']=updateCoord(Alice)
            wallCollision(Alice,aliceData,screenWidth,screenHeight)

            distance=calcDistance(alexData,aliceData)
            won=catchedUp(distance)
            if won==True:
                gameData['won']=True
                turtle.bye()
                congratulate()

            ui(uiTurtle,distance,gameData)
                

        def forward():
            move(Alex,alexData,'forward')
            AInCheck(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData)
        def back():
            move(Alex,alexData,'back')
            AInCheck(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData)

        def left():
            move(Alex,alexData,'turnLeft')
            AInCheck(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData)

        def right():
            move(Alex,alexData,'turnRight')
            AInCheck(Alex,Alice,alexData,aliceData,
                    screenWidth,screenHeight,gameData)


        turtle.onkey(forward,'w')
        turtle.onkey(back,'s')
        turtle.onkey(left,'a')
        turtle.onkey(right,'d')
        turtle.listen()
        turtle.mainloop()


    #create instances of turtles Alice and Alex
    Alice=turtle.Turtle()
    Alice.color('red')
    Alice.pencolor('red')
    Alice.shape("turtle")

    Alex=turtle.Turtle()
    Alex.color('blue')
    Alex.pencolor('blue')
    Alex.shape("turtle")
    uiTurtle=turtle.Turtle()
    #create dictionaries and store data about alice and alex
    alexData=createData(45,30,0,0)


    aliceX,aliceY=randomPos(Alice,screenWidth,screenHeight)
    aliceData=createData(90,20,aliceX,aliceY)
    aliceData['TURNCHANCE']=1
    aliceData['MOVECHANCE']=2

    #general game data
    gameData={
        'won':False,
        'step':1}

    # ! what if Alice spawns within 30px of Alex
    distance=calcDistance(alexData,aliceData)
    ui(uiTurtle,distance,gameData)
    #checks if they spawned too close
    gameData['won']=catchedUp(distance) 

    if gameData['won']==True:
        turtle.bye()
        congratulate()

    userMove(Alex,Alice,alexData,aliceData,
                screenWidth,screenHeight,gameData)


def main():
    '''
        call other functions
    '''
    CanvasHeight=500
    CanvasWidth=500
    CanvasName='Assignment 2 - Chasing Alice'
    setUpCanvas(CanvasWidth,CanvasHeight,CanvasName)
    gameLoop(CanvasWidth,CanvasHeight)


main()