'''    cs-231
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
from random import randint,uniform #for random num

def welcome():
    '''
        welcomes the user to the game
    '''
    print('Welcome to the game of nuts!')

def validMove(move):
    '''
        move is players move
        checks if the move is allowed
    '''
    validity=False

    if 0<move<4: #has to be 1-3 stones
        validity=True
    return validity

def lossCond(nutNum):
    '''
        nutNum-number of nuts left after current players move
        checks if loss condition is met
    '''
    loss=False
    if nutNum<=0: #if number of nuts left is 0 or less current
        #player lost
        loss=True
    return loss

def playerMove(nuts,p=1):
    '''
        nuts-total number of nuts
        p-player by default 1
        processes and checks for validity of player move
    '''
    invalidMove=True #if move by p is invalid prompt again
    while invalidMove:

        invalidInput=True #if input is not valid prompt again
        while invalidInput:
            print('There are %d nuts left on the board'%(nuts))
            nutsTaken=input('Player %d: How many nuts do you want to take (1-3)? '%
                (p))
            try: #if str input instead can crush thats why separate
                #check
                nutsTaken=int(nutsTaken) #is input an int check
                invalidInput=False #get out of loop

            except:
                print('enter an integer between 1 and 3')

        if validMove(nutsTaken): #check player took only 1-3
            invalidMove=False
        else:
            print('enter an integer between 1 and 3')
    print('')
    return nutsTaken 

def pvp(walnutNum):
    '''
        walnutNum-total number of nuts
        player vs player implementation
    '''
    lost=False
    player=2 #we switch them before 1st turn
    while not lost: #if player lost quit loop
        if player==1:
            player=2  #switch players 1->2
        else: 
            player=1 #switch players 2->1

        walnutNum-=playerMove(walnutNum,player)
        #update current nut
        
        lost=lossCond(walnutNum) #see if game lost

    #if lost current player is a loser
    print('Player %d, you lose. \n'%(player)) # state who lost

def initAI(NutNumber):
    '''
        NutNumber-total number of nuts
        code for the AI initiation
        generates naive ai list
    '''
    aiData=[] #init lists for in out of hat data
    outsideAIData=[]

    for hat in range(NutNumber):
        #creates hats equal to the number of nuts
        sublist=[]
        outSublist=[]

        for nutInHat in range(3):#populate sublists
            sublist.append(1)
            outSublist.append(0)

        sublist=sublist[:]    #make sure that not appending ref
        outSublist=outSublist[:]

        aiData.append(sublist) #populate list of hats
        #appends nut counts for the hat to the hat list
        #0,1,2 (index) is number of 1,2,3 nuts by def 1
        outsideAIData.append(outSublist)
        #generates an empty list for storing nuts outside of hat


    return aiData,outsideAIData

def AIMove(baseData,outData,curNutNum):
    '''
        baseData-ai data list with move preferences
        outData-list with 0 matching baseData is length and struct
        curNutNum-current nut number
        write algorythm for moves
    '''
    totalNum=0 #total number of chances 1,2,3 stones are selected

    for counter in baseData[curNutNum-1]: #-1 as baseData starts with [0]
        
        totalNum+=counter #counts it

    #chosenGlobPos=randint(1,totalNum) #choose one num

    ranNum=0 #stores random number
    
    ranNum=uniform(0,1) #better random spread
    ranNum*=100
    ranNum=int(ranNum) #make it int btwn 0-100
    chosenGlobPos=int((totalNum/100)*ranNum+1) #make sure starts
    #at 1
    #print('s-',baseData)
    for j in range(len(baseData[curNutNum-1])):
        #iterate through chance counters
        #iterate through each counter until find desired
        #subtract 1 from that counter
        
        if chosenGlobPos<=baseData[curNutNum-1][j]:
            move=j+1 #if chosenGlobPos index is in current counter
            #take approriate move

            baseData[curNutNum-1][j]-=1 #take the stone out from in hat
            
            outData[curNutNum-1][j]+=1#add stone to out
            break
     
        else:
            chosenGlobPos-=baseData[curNutNum-1][j] #update 
            #globpos based on chance of current action happening
            #compare to next one till match found
    #print('t-',outData)

    return baseData,outData,move #return move and updated lists

def updateAIdata(baseData,outData,win):
    '''
        baseData,outData takes current counters for stones 
        in and out of hat

        mod them based on win/loss

        win=True is victory double taken out add them in
        win=False is defeat discard from base set
        unless one of a kind
    '''
    #!DO NOT UNCOMMEND IF doing training of ai use print in train function instead
    #print ('base=',baseData,'\n out=',outData) #! state of lists after end of match
    
    newBaseData=baseData[:] #not ref
    newOutData=outData[:] #creates clones to be mod based on game

    for i in range(len(baseData)):
        #go through hats
        for j in range(len(outData[i])):
            #go through counters and act
            if outData[i][j]!=0: #if something was taken out
                
                if win==True: #if won double and put back inside
                    newBaseData[i][j]+=2
                    newOutData[i][j]=0 #put a 0 in place
                else:
                    if baseData[i][j]==0:                        
                        newBaseData[i][j]+=1 #prevents a counter being 0
                        
                    newOutData[i][j]=0 #put a 0 in place

   # print ('base-',newBaseData,'\n out-',newOutData) #! lists after manipulation

    return newBaseData,newOutData #return updated lists

def pve(walnutNum,rematch=False,oldAIdata=None,oldAIOutData=None):

    '''
        walnutNum=total num of nuts
        rematch if true init ai data else use old one
        oldAIdata old Aidata
        AI vs Human
        rematch true if AI played a game before vs P or AI
    '''
    #print(oldAIdata,oldAIOutData)
    nutSetting=walnutNum #remember number of nuts for rematch
    if rematch==False:
        AIdata,AIused=initAI(walnutNum) #init ai if 1st game
       
    else:
        AIdata,AIused=oldAIdata,oldAIOutData #use old data
       

    player='AI'
    lost=False

    while not lost:
        if player=='AI': #switch players
            player='Player'
        else:
            player='AI'

        if player=='Player': #process players turn
            walnutNum-=playerMove(walnutNum)
            if walnutNum>0:
                print('There are %d nuts on the board. '%(walnutNum))
        else: #process ai turn
            AIdata,AIused,aiDelta=AIMove(AIdata,AIused,walnutNum)
            #updates both data lists for ai and updates number of walnuts left
            print('AI selects %d \n'%(aiDelta))
            walnutNum-=aiDelta

        lost=lossCond(walnutNum) #see if game lost

    if player=='AI': #AI lost update data
        AIdata,AIused=updateAIdata(AIdata,AIused,False)
    else: #AI won update data
        AIdata,AIused=updateAIdata(AIdata,AIused,True)
    if player=='Player':#notify player who won/lost
        print('You lose. \n')
    else: 
        print('%s lost! \n'%(player)) 
    
    usrRematch=input('Play again ( 1 = yes, 0 = no ) ')

    notSelected=True
    while notSelected: #see if rematch wanted
        #prevents typeo crashing the game
        if usrRematch=='1' or usrRematch=='y':
            #restart the game if yes
            pve(nutSetting,True,AIdata,AIused)
            notSelected=False
        elif usrRematch=='0': #quit loop is no
            notSelected=False
        else:
            print('Please enter 1 or 0') # provide tips if 
            #incorrect input
            usrRematch=input('Play again ( 1 = yes, 0 = no ) ')
            print('')

def train(walnutNum,games):
    '''
        walnutNum-total nut number
        games-games to sim AIvsAI
        runs test games to train up 2nd ai.
        then runs pve with rematch true and passes data
    '''
    
    AIdata,AIused=initAI(walnutNum)
    AI1data,AI1used=initAI(walnutNum) #init AI1 and AI

    percentageStep=1 #every how many % update learning status
    progressUnit=games//100*percentageStep
    progress=0 #learning progress status
    try: 
        #if taking too long ctrl+c to play based on what was
        #learned before stopping
        #time might vary based on hardware

        for game in range(games):
            if game%progressUnit==0: #update status
                os.system('clear') #clear screen to prevent % update
                #spam
                print('Training AI, %d%% complete, press ctrl+c to skip (might make AI worse)'%(progress))
                progress+=1

            currentNutNum=walnutNum
            player='AI'
            lost=False
            
            while not lost:
                if player=='AI': #switch ai
                    player='AI1'
                else:
                    player='AI'

                #process ai step
                if player=='AI1':
                    AI1data,AI1used,aiDelta=AIMove(AI1data,AI1used,currentNutNum)
                    
                else:
                    AIdata,AIused,aiDelta=AIMove(AIdata,AIused,currentNutNum)
                    #updates both data lists for ai and updates number of walnuts left
                   
                currentNutNum-=aiDelta
                    

                lost=lossCond(currentNutNum) #see if game lost

            #update datasets based on outcome of the game
            if player=='AI': #update both lists. current player is loser
                #AI1 won, AI lost 
                AIdata,AIused=updateAIdata(AIdata,AIused,False)
                AI1data,AI1used=updateAIdata(AI1data,AI1used,True)
            else:
                #AI lost, AI won
                AIdata,AIused=updateAIdata(AIdata,AIused,True)
                AI1data,AI1used=updateAIdata(AI1data,AI1used,False)
    
    except KeyboardInterrupt:
        pass #for the impatient or lower hardware specs

    
    os.system('clear') #clear the screen so the games starts with
    #clean scr

    #print('AIlist: ',AIdata) #! uncomment prints second ai
    #print('') #!uncomment
    #print('AI1list: ',AI1data) #! uncomment prints 1st ai
    
    pve(walnutNum,True,AIdata,AIused) #start a match against human


def select():
    '''
        select which mode to play
    '''
    initialNutCount=False

    while not initialNutCount:
        nutNum=input('How many nuts are there on the table initially (10-100)? ')
        nutNum=int(nutNum) #prompt to get nut num and process it
        if 10<=nutNum<=100: #check boundaries
            initialNutCount=True
        else: #provide tip if incorrect input
            print('Please enter a number between 10 and 100')
    
    invalid=True
    while invalid: #prompt user to select a version of the game
        #to play
        print('Options:')
        print('Play against a friend (1)')
        print('Play against the computer (2)')
        print('Play against the trained computer (3)')

        usrI=input('Which Option do you take (1-3)? ')
        print('')

        if usrI=='1':
            pvp(nutNum)
        elif usrI=='2':
            pve(nutNum)
        elif usrI=='3':
            TrainGamesNum=100000
            train(nutNum,TrainGamesNum)
        else: #tips
            print('enter a number between 1 and 3')


def main():
    '''
        starts the game
    '''
    welcome()
    select()

main()