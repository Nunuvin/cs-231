Given text(s) the program creates semantic descriptors of words and then tries it's
best to find best match/synonim to the word provided and then compares to the right
answer.
Originally was written and tested with 2 texts in mind:
War and peace, Swann's Way
These texts have to be named Tolstoy.txt and Proust.txt respectivly and placed in
same folder as the main program.
Default test file is test.txt
links to download:
http://www.gutenberg.org/cache/epub/7178/pg7178.txt
http://www.gutenberg.org/cache/epub/2600/pg2600.txt