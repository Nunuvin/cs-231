'''made by Vlad C.'''

'''Semantic Similarity: starter code

Author: Michael Guerzhoy. Last modified: Nov. 18, 2015.
'''

import math
from time import time

def norm(vec):
    '''Return the norm of a vector stored as a dictionary,
    as described in the handout for Project 3.
    '''

    sum_of_squares = 0.0  # floating point to handle large numbers
    for x in vec:
        sum_of_squares += vec[x] * vec[x]

    return math.sqrt(sum_of_squares)

def timeFunc(f):
    '''
        adapted from What Does It Take To Be An Expert At Python?
        https://www.youtube.com/watch?v=7lmCu8wz8ro mentioned somewhere
        between 45mins-60mins into the video. Added it in as it is not an
        important/essential part of the code for the assignment
        decorator function. Takes in function f and runs it in wrapper
    '''
    def wrapper(*args):
        '''
            times function f
        '''
        start = time()
        rv = f(*args)
        finish = time()
        print('Time taken: ', finish - start)
        return rv
    return wrapper



def cosine_similarity(vec1, vec2):
    '''
        finds cosine similarity between 2 vectors given as vec1 and vec2
        returns the finding
    '''
    denominator = norm(vec1) * norm(vec2)
    numerator = 0
    for key in vec1.keys():
        if key in vec2.keys():
            numerator += vec1[key] * vec2[key]
    return numerator / denominator


def build_semantic_descriptors(sentences):
    '''
        given a list of sentences with sentences represented as lists
        creates semantic descriptor for each word mentioned and stores
        in a dictionary which will be returned
    '''
    semanticDescription = {}
    for sentence in sentences:
        for i in range(len(sentence)): #go through words in sentence
            if not (sentence[i] in semanticDescription.keys()): #if word unique
                semanticDescription[sentence[i]] = {}
            for word in (sentence[:i] + sentence[i+1:]): #in sentence except cur
                if not (word in semanticDescription[sentence[i]].keys()):#word
                    semanticDescription[sentence[i]][word] = 1 #new entry to
                else: #semantic descriptor
                    semanticDescription[sentence[i]][word] += 1 #add to existing
    return semanticDescription


def generate_sentences(text):
    '''
        generates from text list of sentences as lists and returns them
    '''
    punctuation = [',', '-', ':', ';', '"', "'"]
    separators = ['.', '!', '?']
    sentence = []
    sentenceList = []
    for word in text:
        word = word.lower()
        newS = False
        for p in punctuation:
            if p in word:
                word = word.replace(p,'') #remove punctuation
        for s in separators:
            if s in word:
                word = word.replace(s,'')
                if word: #if its end of sentence
                    sentence.append(word)
                sentenceList.append(sentence)
                sentence = []
                newS = True
                break
        if word and not newS: #add to the current sentence
            sentence.append(word)
    return sentenceList

#@timeFunc #~!
def build_semantic_descriptors_from_files(filenames):
    '''
        builds semantic descriptors from files provided
    '''
    data=[]
    for fileProvided in filenames:
        with open(fileProvided, 'r', encoding = "utf-8") as file:
            data += file.readlines() #file with all lines
    newData=[]
    for line in data:
        newData += line.split() #separate words
    sentenceList = generate_sentences(newData)
    semanticDescriptions = build_semantic_descriptors(sentenceList)
    return semanticDescriptions


def most_similar_word(word, choices, semantic_descriptors, similarity_fn):
    '''
        finds most similar word to word from choices avaliable based on
        coef returned from similarity_fn with semantic_descriptors of the word
        as parameters
        returns the most similar word with lowest index
        the word must be in the semantic data_base or None is returned
    '''
    if word not in semantic_descriptors:
        return None
    wordSemantic = semantic_descriptors[word]
    comparison = []
    for c in choices:
        if c not in semantic_descriptors.keys():
            continue #skip choices with no semantic descriptors
        #print(similarity_fn(wordSemantic, semantic_descriptors[c]))
        comparison.append(similarity_fn(wordSemantic, semantic_descriptors[c]))
    lowestIndex = 0
    for i in range(1, len(comparison)):
        if comparison[i] > comparison[lowestIndex]:
            lowestIndex = i
    return choices[lowestIndex]


def run_similarity_test(filename, semantic_descriptors, similarity_fn):
    '''
        loads test file and calls similarity func which compares semantic
        descriptors of choices and word it self to find the best match
    '''
    with open(filename,'r', encoding = 'utf-8') as file:
        tests = file.readlines()
    totalTests = 0
    correct = 0
    for line in tests:
        test = line.split()
        guess=most_similar_word(test[0], test[2:], semantic_descriptors,
                similarity_fn)
        if guess!=None and guess in semantic_descriptors.keys():
            #guess is valid and in the database
            if test[1] in semantic_descriptors.keys():
                #the right answer is in database too
                totalTests+=1
                if test[1] == guess:
                    correct += 1
    #print(totalTests)
    simValue = format(correct / totalTests*100, '.1f')
    #print (simValue)
    return simValue

#test functions!


def test():
    '''
        tests code
    '''
    #read sources and create semantic descriptors
    SDesc=build_semantic_descriptors_from_files(['Proust.txt','Tolstoy.txt'])
    #print(len(SDesc))
    #pass them to the similarity test function
    value=run_similarity_test('test.txt',SDesc,cosine_similarity)
    print(value) #print value

#test() #~!
