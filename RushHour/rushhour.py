'''    cs-231
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import sys


class car():
    direction=None
    red=False #red car is the target car
    cellOccupied=[]

    def __init__(self,data):
        '''
            data=[direction,length,x,y]
            direction - h/v
            occupied cells occupied by the car
        '''
        self.direction=data[0]
        self.cellOccupied=[]
        for i in range(data[1]):
            if self.direction=='v':
                self.cellOccupied.append([data[3],data[2]+i])
            elif self.direction=='h':
                self.cellOccupied.append([data[3]+i,data[2]])

    def move(self,distance):
        '''
            moves car for the distance specified
            negative distance is moving back
        '''
        for cell in self.cellOccupied:
            if self.direction=='h':
                cell[0]+=distance
            elif self.direction=='v':
                cell[1]+=distance

class lot():
    '''
        level data and all operations on the level
    '''
    carList=[]
    solved=False #gamestate

    def readfile(self):
        '''
            reads file provided in the arg
        '''
        if len(sys.argv)>1: #checks that file is provided
            with open(sys.argv[1],'r') as gameMap:
                carData=gameMap.readlines()
        newCar=[]
        newData=[]
        for car in carData: #reformat red list so each car is a
            car=car.strip()#remove new lines
            newCar=car.split(',')#sublist and each entry is separate
            newData.append(newCar)
        for car in newData:
            car[0]=car[0].lower() #converts h/v into lowercase
            for i in range(len(car)): #converts all other entries into int
                if i!=0:
                    car[i]=int(car[i])
        #print(newData)
        return newData

    def validateMap(self):
        '''
            checks if map data provided is in acceptable range and complete
            returns true if valid
        '''
        
        entries=[]
        for carEntry in self.mapData:
            valid=True
            if len(carEntry)!=4: #checks that all data is present [dir,len,x,y]
                valid=False
            elif carEntry[0]!='h' and carEntry[0]!='v':#dir is correct
                valid=False
            elif carEntry[1]<1 or carEntry[1]>6: 
                #car cant be longer than grid or shorter than 1
                valid=False
            else:
                entries.append(carEntry)
            if not valid: #if entry is not valid terminate the game
                print(carEntry)
                print('invalid car(s) are present and ignored! make sure it is properly ')
                print('formatted and values are in acceptable range')
            self.mapData=entries[:]
        return valid

    def checkWinCondition(self):
        '''
            checks if the red car is at the exit of the lot
            
        '''
        won=False
        for carl in self.carList: #if this is a first car
            if carl.red==True:
                for part in carl.cellOccupied: #and one of its parts is 
                    if part==[5,2]: #exit square state that puzzle is solved
                        self.solved=True
                break
            
    def processInput(self,pmove):
        '''
            processes player move
            expected input  carObj index MOVE
            move + fwrd
            move - bck
            returns true if valid
        '''
        valid=True
        for step in range(abs(pmove[2])): #try moving 1 unit
            #move 1 unit
            if pmove[2]<0: #neg move
                pmove[0].move(-1)
            else:
                pmove[0].move(1)
            #check if it collides with anything
            for cell in pmove[0].cellOccupied:
                if not (0<=cell[0]<6 and 0<=cell[1]<6):
                    valid=False
                for c in self.carList:
                    if c!=pmove[0]:
                        for cel in c.cellOccupied:
                            if cel==cell:
                                valid=False
            #if so move 1 unit back break
            if not valid: 
                if pmove[2]<0: #neg move
                    pmove[0].move(1)
                elif pmove[2]>0:
                    pmove[0].move(-1)
                break
            self.checkWinCondition()
            #check win cond

    def checkCarRange(self):
        '''
            checks if car is in grid range
        '''
        tempList=[]
        for carI in self.carList:
            validCar=True
            for cell in carI.cellOccupied:
                if not(0<=cell[0]<6 and 0<=cell[1]<6):
                    validCar=False
            if validCar:
                tempList.append(carI)
        self.carList1=tempList[:]

    def checkCarOverlap(self):
        '''
            checks if cars overlap
            if so closest to beginning of the list stays
        '''
        refinedCars=[]
        for c in range(len(self.carList)):
            unique=True
            if c==0:
                refinedCars.append(self.carList[c])
            else:
                for j in range(0,c):
                    for cell in self.carList[c].cellOccupied:
                        for carCell in self.carList[j].cellOccupied:
                            if carCell==cell:
                                unique=False
                                #if there was a car with exactly same grid coord occupied ignore this car
                if unique:
                    refinedCars.append(self.carList[c])
        self.carList=refinedCars[:]



    def __init__(self):
        '''
            fills lot with empty mx
        '''
        self.mapData=self.readfile()
        self.validateMap()        
        for carD in range(len(self.mapData)):
            self.carList.append(car(self.mapData[carD]))
            if carD==0:
                self.carList[0].red=True

        self.checkCarRange()
        self.checkCarOverlap()

class interface():
    '''
        deals with input output
    '''
    ConsoleLot=[]

    def ConsoleWelcome(self):
        print('Welcome to the game Rush Hour!')

    def ConsoleCongratulate(self):
        print('Congratulations You solved the puzzle!')


    def ConsoleFillEmpty(self):
        '''
            fills lot with emptiness and fences
        '''
        interface.ConsoleLot=[]
        for i in range(6):
            row=[]
            for j in range(6):
                if i==2 and j==5:
                    row.append('>')
                else:
                    row.append('.')#free space
            interface.ConsoleLot.append(row) #lot.lot as there is only one stage
            # possible at a time

    def ConsoleFillCars(self):
        '''
            adds cars to the matrix
            takes each car from carL and places symbol in the mx for each cell
             car occupies
        '''
        carID='@ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' #@ is the red car
        for i in range(len(self.gameLot.carList)):
            for cell in self.gameLot.carList[i].cellOccupied:# go through each list of dots and
                interface.ConsoleLot[cell[1]][cell[0]]=carID[i] #place in Mx car ID 
                #on coord specified
        #print('hi',interface.ConsoleLot)

    def ConsolePrintCars(self):
        '''
            prints matrix to console + surrounds the matrix with # to be fences
             and prints coord sys num as first row and letters as columns
        '''
        rows=len(interface.ConsoleLot)+3 # coord sys row + fence + fence
        columns=len(interface.ConsoleLot[0])+3
        rowID=list('ABCDEF')
        for row in range(rows):
            if row==0:
                for column in range(columns):
                    if column!=0 and column!=1 and column!=columns-1:
                        print(column-2, end='') #print column nums
                    else:
                        print(' ',end='')    
            elif row==1 or row==rows-1: #~! interface.ConsoleLot is not a list
                for column in range(columns):
                    print('#',end='')#draw fence
            else:
                for column in range(columns):
                    if column==0:
                        print(rowID[row-2],end='') #prints letter of rows
                    elif column==1 or column==columns-1:
                        print('#',end='') #print fence
                    else:
                        print(interface.ConsoleLot[row-2][column-2],end='')#print grid

            print() #newline

    def ConsoleReadInput(self):
        ''' 
            converts player input into a grid coord and distance to move
            x,y +dir or x,y -dir +/- is fwd/bck
            output obj,move
        '''
        colID='ABCDEF'
        usrIn=input('what is your next move? (format: X Y move) ')
        usrIn=usrIn.split()
        movedCar=[]
        if len(usrIn)==3: #check imput size
            x=colID.find(usrIn[0].upper())#find x coord in colID
            try:
                y=int(usrIn[1]) #convert data to int if possible
                move=int(usrIn[2])
            except Exception:
                print('Invalid input ')
                y=-1
                move=None
        else:
            print('Invalid input ')
            x=-1
            y=-1
            move=None
        if x!=-1 and y!=-1 and move!=None: #proper input
            for j in range(len(self.gameLot.carList)): #id the car
                for cell in self.gameLot.carList[j].cellOccupied:
                    if [y,x]==cell:
                        movedCar=self.gameLot.carList[j]
                        carNum=j
                        return movedCar,carNum,move
        return self.ConsoleReadInput()

    def __init__(self,uiType):
        if uiType=='Console':
            self.ConsoleWelcome()
            self.gameLot=lot()
            while not self.gameLot.solved:
                self.ConsoleFillEmpty()
                self.ConsoleFillCars() #sets up the mx
                self.ConsolePrintCars() #displayes the mx
                self.gameLot.processInput(self.ConsoleReadInput()) #read input
                if self.gameLot.solved:
                    self.ConsoleFillEmpty()
                    self.ConsoleFillCars() #sets up the mx
                    self.ConsolePrintCars() #displayes the mx
                    self.ConsoleCongratulate()


def main():
    runConsole=interface('Console')

if __name__=="__main__":
    main()

