'''
    cs-231
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

#import used libraries and the main game
import pygame,sys
from pygame.locals import *
from rushhour import *


pygame.init() #init pygame
guiSpace=100 #reserves 0-99y as space for turn counter
pyWidth,pyHeight=480,480+guiSpace
pySurf=pygame.display.set_mode((pyWidth,pyHeight)) #setup window and caption on it
pygame.display.set_caption('Rush Hour')

BLACK =(0,0,0) #setup tuples for colors
GREY = (127,127,127)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)



font = pygame.font.SysFont(None, 32)



class pyGUI(interface):
    turn=0 #turn counter
    cellSize=80 #side of a grid cell
    carBorder=5 #how much the car should be spaced out from side of cell
    def displayTurn(self):
        '''prints move counter at top right corner'''
        displayTurn = font.render('Turn: %d'%(self.turn), True, WHITE, BLACK)
        textRect = displayTurn.get_rect()
        textRect.centerx = 50 #sets the render pos
        textRect.centery = 50
        self.surface.blit(displayTurn,[50,50])
        
    
    def carLengthVector(self):
        '''calculates the difference between x,y of first and last car'''
        carVlist=[]
        for car in self.gameLot.carList: #calc for all cars
            Vx,Vy=car.cellOccupied[-1][0]-car.cellOccupied[0][0],car.cellOccupied[-1][1]-car.cellOccupied[0][1]
            carVlist.append([Vx,Vy])
        return carVlist

    def drawImage(self):
        '''draws the lot'''
        self.surface.fill(GREY)
        pygame.draw.rect(self.surface, BLACK, (0,0,self.surface.get_width(),guiSpace)) #space for gui
        pygame.draw.rect(self.surface, GREEN,(5*pyGUI.cellSize,2*pyGUI.cellSize+guiSpace,pyGUI.cellSize,pyGUI.cellSize)) #win square
        self.displayTurn()
        carlength=self.carLengthVector()
        for cars,length in zip(self.gameLot.carList,carlength):
            if cars.red==True:
                color=RED #red car is red
            else:
                color=BLUE #all other cars
            #need to change so  there is space between cars
            if cars.direction=='v': #draw cars based on where they are facing to determine where the car border should be at the start and end of the car
                pygame.draw.rect(self.surface,color,(cars.cellOccupied[0][0]*pyGUI.cellSize+pyGUI.carBorder,cars.cellOccupied[0][1]*pyGUI.cellSize+guiSpace+pyGUI.carBorder,(length[0]+1)*pyGUI.cellSize-pyGUI.carBorder,(length[1]+1)*pyGUI.cellSize-pyGUI.carBorder))
            elif cars.direction=='h':
                pygame.draw.rect(self.surface,color,(cars.cellOccupied[0][0]*pyGUI.cellSize+pyGUI.carBorder,cars.cellOccupied[0][1]*pyGUI.cellSize+guiSpace+pyGUI.carBorder,(length[0]+1)*pyGUI.cellSize-pyGUI.carBorder,(length[1]+1)*pyGUI.cellSize-pyGUI.carBorder))
        pygame.display.update()
    
    def mouseCarSelect(self):
        '''
            get mouse position and convert it into grid coord then id car
            return car obj and its index
        '''
        mousePos=list(pygame.mouse.get_pos())
        mousePos[1]-=guiSpace #removes the displacement due to gui
        carPos=[mousePos[0]//pyGUI.cellSize,mousePos[1]//pyGUI.cellSize]
        for cars in range(len(self.gameLot.carList)):
            for c in self.gameLot.carList[cars].cellOccupied:
                if c==carPos:
                    return self.gameLot.carList[cars],cars
        return None

    def convertMousePos(self):
        ''' return mouse pos in grid coord'''
        mousePos=list(pygame.mouse.get_pos())
        mousePos[1]-=guiSpace
        Pos=[mousePos[0]//pyGUI.cellSize,mousePos[1]//pyGUI.cellSize]
        return Pos

    def __init__(self,uiType,surface,guiSpace):
        if uiType=='Pygame': #pygame!
            self.surface=surface #init var and obj
            self.guiSpace=guiSpace
            self.gameLot=lot()
            carSelected=False
            oldPos=[0,0]
            while not self.gameLot.solved:
                for event in pygame.event.get():
                    if event.type == QUIT: #if you want to quit the game press big x button
                        pygame.quit()
                        sys.exit()
                    elif event.type==MOUSEBUTTONDOWN and not carSelected: #if no car selected and mouse button is down select one clicked on
                        chosenCar=self.mouseCarSelect()
                        if chosenCar!=None:
                            oldPos=self.convertMousePos()
                            carSelected=True
                    elif event.type==MOUSEBUTTONUP: #get the pos to where you want to drag the car to
                        newPos=self.convertMousePos()
                        xChange=newPos[0]-oldPos[0]
                        yChange=newPos[1]-oldPos[1]
                        if xChange!=0 and not yChange!=0 and chosenCar!=None: #try moving car based on input
                            #hor change
                            self.gameLot.processInput([chosenCar[0],chosenCar[1],xChange])
                            self.turn+=1
                            carSelected=False
                        elif not xChange!=0 and yChange!=0 and chosenCar!=None:
                            #ver change
                            self.gameLot.processInput([chosenCar[0],chosenCar[1],yChange])
                            self.turn+=1
                            carSelected=False
                    self.drawImage() #update screen

            print('You have solved the puzzle in %d turns'%(self.turn))
            pygame.quit()
            sys.exit()

def main():
    runGUI=pyGUI('Pygame',pySurf,guiSpace)


if __name__=='__main__':
    main()